# Aliases
alias gl="git log --oneline --color --graph"
alias gs="git status -uno"
alias gco="git checkout"
alias ab="cd ~; cd Desktop; cd Projects; cd accessbraille;"
alias ho="cd ~;"
alias ls="ls -G"
alias vimb="cd ~; vim .bashrc"
alias vimv="cd ~; vim .vimrc"
alias gco="git checkout"
alias pr="cd ~/Desktop/Projects"
alias vims="cd ~/.ssh/config" 
alias gb="git branch"

# Colors
export TERM="xterm-color"
PS1='\[\e[0;33m\]\u\[\e[0m\]@\[\e[0;32m\]\h\[\e[0m\]:\[\e[0;34m\]\w\[\e[0m\]\$ '
